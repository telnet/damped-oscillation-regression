import numpy as np
from scipy.optimize import least_squares
import matplotlib.pyplot as plt
T=10
N=300
init_val=[0,0]
def generate_data(m,kc,ke,b):#sigma=-b/2m
	y=np.zeros(N)
	t=np.linspace(0,T,N)
	dt=t[1]-t[0]
	y[0],y[1]=tuple(init_val)
	for i in range(N-2):
		if(y[i]>=0):
			k_real=kc
		else:
			k_real=ke
		denominator=m/dt**2+b/(2*dt)
		numerator=(2*m/dt**2)*y[i+1]-(m/dt**2-b/(2*dt)+k_real)*y[i]
		y[i+2]=numerator/denominator
	return t,y
def error(hyper_params,t,y):
	m,kc,ke,b=hyper_params
	_t,_y=generate_data(m,kc,ke,b)
	err=_y-y
	return err
fp=open("cal.csv","r")
t_train=[]
y_train=[]
print("[*] Loading data...")
for i,l in enumerate(fp):
	if(i==0):
		continue
	tmp=l.strip().split(",")
	t_train.append(float(tmp[0]))
	y_train.append(float(tmp[2]))
hyper_params=np.array([1.0,1.0,1.0,1.0])
print("[+] Loaded data. (size {} rows.)".format(len(t_train)))
print("[*] Initiating regression.")
t_train=np.array(t_train)
y_train=np.array(y_train)
init_val=[y_train[0],y_train[1]]
res_lsq = least_squares(error, hyper_params, args=(t_train,y_train))
print("[+] Completed regression.")
t_sim,y_lsq = generate_data(*res_lsq.x)

out_fd=open("hyperparams.txt","w")
for x in res_lsq.x:
	out_fd.write(str(x))
	out_fd.write("\n")

out_fd.close()
plt.plot(t_train, y_train, 'o', label='data',markersize=2,color='black')
plt.plot(t_sim, y_lsq, '-', label='fit',color='red')
plt.xlabel('$t$')
plt.ylabel('$y$')
plt.legend()
print("[*] Plotting...")
plt.show()