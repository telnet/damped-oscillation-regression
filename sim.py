import matplotlib.pyplot as plt
import numpy as np
k=10
m=1
N=1000
b=1
t=np.linspace(0,10,N)
dt=t[1]-t[0]
y=np.zeros(N)
y_1=np.zeros(N)
A=10
y_1[0]=-A
y_1[1]=-A+dt*(k*A/m)
y[0]=-A
y[1]=-A+dt*(k*A/m)
k_real=k
for i in range(N-2):
	denominator=m/dt**2+b/(2*dt)
	numerator=(2*m/dt**2)*y_1[i+1]-(m/dt**2-b/(2*dt)+k_real)*y_1[i]
	y_1[i+2]=numerator/denominator
for i in range(N-2):
	if(y[i]>=0):
		k_real=0.5*k
	else:
		k_real=k
	denominator=m/dt**2+b/(2*dt)
	numerator=(2*m/dt**2)*y[i+1]-(m/dt**2-b/(2*dt)+k_real)*y[i]
	y[i+2]=numerator/denominator
plt.plot(t, y_1, '-', label='spring model',color='red')
plt.plot(t, y, '-', label='rubber band model',color='green')
plt.xlabel('$t$')
plt.ylabel('$y$')
plt.legend()
plt.show()