import numpy as np
from scipy.optimize import least_squares
import matplotlib.pyplot as plt

def generate_data(t, A, sigma, omega, C, phi):#sigma=-b/2m
    y = A * np.exp(-sigma * t) * np.cos(omega * t+phi)+C
    return y
	
def error(hyper_params,t,y):
	A,sigma,omega,C,phi=hyper_params
	err= A * np.exp(-sigma * t) * np.cos(omega * t+phi)+C-y
	return err
fp=open("data.csv","r")
t_train=[]
y_train=[]
print("[*] Loading data...")
for i,l in enumerate(fp):
	if(i==0):
		continue
	tmp=l.strip().split(",")
	t_train.append(float(tmp[0]))
	y_train.append(float(tmp[1]))
hyper_params=np.array([1.0,1.0,1.0,1.0,1.0])

print("[+] Loaded data. (size {} rows.)".format(len(t_train)))
print("[*] Initiating regression.")
t_train=np.array(t_train)
y_train=np.array(y_train)
res_lsq = least_squares(error, hyper_params, args=(t_train,y_train))
print("[+] Completed regression.")
y_lsq = generate_data(t_train, *res_lsq.x)

out_fd=open("hyperparams.txt","w")
for x in res_lsq.x:
	out_fd.write(str(x))
	out_fd.write("\n")

out_fd.close()
plt.plot(t_train, y_train, 'o', label='data',markersize=2,color='black')
plt.plot(t_train, y_lsq, '-', label='fit',color='red')
plt.xlabel('$t$')
plt.ylabel('$y$')
plt.legend()
print("[*] Plotting...")
plt.show()